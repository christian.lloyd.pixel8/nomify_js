// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { initializeAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDTxcDoWQFrQM6tsqd7X7lbtGYj7lfk4-4",
  authDomain: "nomify-auth-5a19c.firebaseapp.com",
  projectId: "nomify-auth-5a19c",
  storageBucket: "nomify-auth-5a19c.appspot.com",
  messagingSenderId: "567238669434",
  appId: "1:567238669434:web:6767ea692cc3e41b5177c3",
  measurementId: "G-B5R9T03CEP",
};

let auth;

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

auth = initializeAuth(app);

export default auth;
