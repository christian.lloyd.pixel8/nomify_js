import { StyleSheet, Text, View, Pressable } from "react-native";
import React from "react";

const CustomButton = ({
  onPress,
  text,
  type = "PRIMARY",
  bgColor,
  fgColor,
}) => {
  return (
    <Pressable
      onPress={onPress}
      style={[
        styles.container,
        styles[`container_${type}`],
        bgColor ? { backgroundColor: bgColor } : {},
      ]}
    >
      <Text
        style={[
          styles.text,
          styles[`text_${type}`],
          fgColor ? { color: fgColor } : {},
        ]}
      >
        {text}
      </Text>
    </Pressable>
  );
};

export default CustomButton;

const styles = StyleSheet.create({
  container: {
    width: "100%",
    padding: 15,
    marginVertical: 5,
    alignItems: "center",
    borderRadius: 10,
  },
  container_PRIMARY: { backgroundColor: "#98e365" },
  container_SECONDARY: { backgroundColor: "#3B71F3" },
  container_TRETIARY: {},
  container_FOURTH: { borderColor: "#98e365", borderWidth: 2 },

  text: { fontWeight: "bold", color: "white" },
  text_FOURTH: { color: "#98e365" },
  text_TRETIARY: {
    color: "gray",
  },
});
