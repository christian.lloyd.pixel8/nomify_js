import { View, Text } from "react-native";
import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import SignInScreen from "../screens/SignInScreen";
import HomeScreen from "../screens/HomeScreen";
import Ingredients from "../screens/Ingredients";
import ForgotPassword from "../screens/ForgotPassword";
import SignUpScreen from "../screens/SignUpScreen";
import NewPasswordScreen from "../screens/NewPasswordScreen";
import ConfirmEmailScreen from "../screens/ConfirmEmailScreen";
import NormalScreen from "../screens/Normal";
import UnderweightScreen from "../screens/Underweight";
import OverweightScreen from "../screens/Overweight";
import ObeseScreen from "../screens/Obese";

const Stack = createNativeStackNavigator();

export default function AppNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="SignIn"
        screenOptions={{
          headerShown: false,
        }}
      >
        <Stack.Screen name="SignIn" component={SignInScreen} />
        <Stack.Screen name="Forgot" component={ForgotPassword} />
        <Stack.Screen name="SignUp" component={SignUpScreen} />
        <Stack.Screen name="NewPass" component={NewPasswordScreen} />
        <Stack.Screen name="Confirm" component={ConfirmEmailScreen} />

        <Stack.Screen name="Ingredients" component={Ingredients} />
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen
          name="Underweight"
          component={UnderweightScreen}
          screenOptions={{
            headerShown: true,
          }}
        />
        <Stack.Screen
          name="Normal"
          component={NormalScreen}
          screenOptions={{
            headerShown: true,
          }}
        />
        <Stack.Screen
          name="Overweight"
          component={OverweightScreen}
          screenOptions={{
            headerShown: true,
          }}
        />
        <Stack.Screen
          name="Obese"
          component={ObeseScreen}
          screenOptions={{
            headerShown: true,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
