import React, { useState } from "react";
import { View, Text, StyleSheet, Alert } from "react-native";
import { useNavigation } from "@react-navigation/native";
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import Underweight from "./Underweight";

const HomeScreen = ({ navigation }) => {
  const [weight, setWeight] = useState("");
  const [height, setHeight] = useState("");
  const [age, setAge] = useState("");
  const [gender, setGender] = useState("");

  const calculateBMI = () => {
    // Validate inputs
    if (
      !weight ||
      !height ||
      !age ||
      isNaN(weight) ||
      isNaN(height) ||
      isNaN(age)
    ) {
      Alert.alert(
        "Invalid Input",
        "Please enter valid numeric values for all fields."
      );
      return;
    }

    const weightInKg = parseFloat(weight);
    const heightInMeters = parseFloat(height) / 100;
    const bmi = weightInKg / (heightInMeters * heightInMeters);

    if (bmi >= 1 && bmi <= 18.4) {
      console.log("Underweight", bmi);
      navigation.navigate(Underweight);
    } else if (bmi >= 18.5 && bmi < 24.9) {
      console.log(bmi);
      navigation.navigate("Normal");
    } else if (bmi >= 25 && bmi < 29.9) {
      console.log(bmi);
      navigation.navigate("Overweight");
    } else {
      console.log(bmi);
      navigation.navigate("Obese");
    }
  };

  return (
    <View style={styles.container}>
      <Text>Calculation of BMI</Text>
      <Text>Weight (kg):</Text>
      <CustomInput
        style={styles.input}
        value={weight}
        setValue={setWeight}
        keyboardType="numeric"
      />
      <Text>Height (cm):</Text>
      <CustomInput
        style={styles.input}
        value={height}
        setValue={setHeight}
        keyboardType="numeric"
      />
      <Text>Age:</Text>
      <CustomInput
        style={styles.input}
        value={age}
        setValue={setAge}
        keyboardType="numeric"
      />
      <CustomButton text="Calculate BMI" onPress={calculateBMI} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
  },
});

export default HomeScreen;
