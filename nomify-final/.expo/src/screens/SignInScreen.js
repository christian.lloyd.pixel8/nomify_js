import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  useWindowDimensions,
  ScrollView,
  Alert,
} from "react-native";
import Logo from "../../../assets/images/nomify-logo.png";
import icon from "../../../assets/images/logo.jpg";
import React, { useState } from "react";
import { useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import LottieView from "lottie-react-native";
import Animated from "react-native-reanimated";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import CustomInput from "../components/CustomInput";
import CustomButton from "../components/CustomButton/CustomButton";
import SocialSignInButton from "../components/SocialSignInButton/SocialSignInButton";

export default function SignInScreen() {
  const navigation = useNavigation();

  const [email, setEmail] = useState("");

  const [password, setPassword] = useState("");

  const [passwordVisible, setPasswordVisible] = useState(false);

  const { height } = useWindowDimensions();

  const handleSignIn = () => {
    if (!email.trim() || !password.trim()) {
      Alert.alert("Error", "Please enter both email and password.");
      return;
    }

    const emailRegex = /\@gmail\.com$/;

    if (!emailRegex.test(email)) {
      Alert.alert("Error", "Please enter a valid email address.");
      return;
    }
    navigation.navigate("Home");
    console.log("Sign in with", email, password);
  };

  const handlePress = () => {
    handleSignIn();
  };

  const onSignInPressed = () => {
    console.warn("Sign In");
  };

  const onForgotPressed = () => {
    console.warn("Forgot");
  };

  const onSignUpPressed = () => {
    console.warn("Signup");
  };

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.root}>
        <Image source={Logo} />
        <Image source={icon} style={[styles.logo, { height: height * 0.2 }]} />
        <CustomInput placeholder="Email" value={email} setValue={setEmail} />
        <CustomInput
          placeholder="Password"
          value={password}
          setValue={setPassword}
          secureTextEntry={!passwordVisible}
        />

        <TouchableOpacity
          onPress={() => setPasswordVisible(!passwordVisible)}
          style={styles.passicon}
        >
          <Ionicons name={passwordVisible ? "eye" : "eye-off"} size={24} />
        </TouchableOpacity>

        <CustomButton text="Sign In" onPress={handlePress} />
        <CustomButton
          text="Forgot Password?"
          onPress={() => navigation.navigate("Forgot")}
          type="TRETIARY"
        />
        <SocialSignInButton />
        <CustomButton
          text="Don't have an account Create Now"
          onPress={() => navigation.navigate("SignUp")}
          type="TRETIARY"
        />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  root: {
    alignItems: "center",
    paddingBottom: 20,
    paddingLeft: 20,
    paddingRight: 20,
  },
  logo: {
    width: "43%",
    maxWidth: 300,
    maxheight: 200,
    borderRadius: 20,
    marginTop: 40,
    marginBottom: 50,
  },
  passicon: {
    position: "relative",
    bottom: 43,
    left: 150,
  },
});
