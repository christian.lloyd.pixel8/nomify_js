import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  Image,
} from "react-native";
import { useRoute, useNavigation } from "@react-navigation/native";
import { Ionicons } from "@expo/vector-icons";
import mealData from "./ObeseData";

const Ingredients = () => {
  const route = useRoute();
  const navigation = useNavigation();
  const mealName = route.params.meal;
  let mealDataItem;

  // Use Object.entries to iterate over the mealData object and find the matching meal
  const [mealType, meals] = Object.entries(mealData).find(([_, meals]) =>
    meals.some((meal) => meal.name.toLowerCase() === mealName.toLowerCase())
  );
  if (mealType) {
    mealDataItem = meals.find(
      (meal) => meal.name.toLowerCase() === mealName.toLowerCase()
    );
  }

  return (
    <ScrollView style={styles.container}>
      <View style={styles.mealImageContainer}>
        <Image source={mealDataItem.image} style={styles.mealImage} />
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.backButton}
        >
          <Ionicons name="arrow-back" size={24} color="white" />
        </TouchableOpacity>
      </View>
      <View style={styles.mealInfoContainer}>
        <Text style={styles.mealName}>{mealName}</Text>
      </View>
      <View style={styles.ingredientsContainer}>
        <Text style={styles.ingredientsLabel}>Ingredients:</Text>
        {mealDataItem &&
          mealDataItem.ingredients.map((ingredient, index) => (
            <View key={index} style={styles.ingredientItem}>
              <Text style={styles.ingredientText}>• {ingredient}</Text>
            </View>
          ))}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mealImageContainer: {
    position: "elative",
  },
  mealImage: {
    width: "100%",
    height: 300,
    backgroundColor: "#ccc",
  },
  backButton: {
    position: "absolute",
    top: 30,
    left: 10,
    backgroundColor: "black",
    padding: 10,
    borderRadius: 25,
    marginTop: 20,
  },
  mealInfoContainer: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    marginTop: -20,
  },
  mealName: {
    fontSize: 30,
    fontWeight: "bold",
    textAlign: "center", // Add this line
    marginTop: 20,
  },
  ingredientsContainer: {
    backgroundColor: "white",
    borderRadius: 10,
    padding: 20,
    height: 500,
  },
  ingredientsLabel: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  ingredientItem: {
    marginBottom: 10,
    marginLeft: 30,
  },
  ingredientText: {
    fontSize: 15,
  },
});

export default Ingredients;
